package message

import "manosecli/cmd/printout/colour"

const (
	NeedNoteID                 = colour.ColorRed + "\n\tВведите id записи.\n" + colour.ColorReset
	NeedPathID                 = colour.ColorRed + "\n\tВведите path для отображения записей.\n" + colour.ColorReset
	WrongNoteID                = colour.ColorRed + "\n\tТакой записи не существует.\n" + colour.ColorReset
	NeedToChooseNoteOrReminder = colour.ColorRed + "\n\tВы не выбрали записки или напоминания.\n\tПривер: \"note show\" или \"reminder show\"\n" + colour.ColorReset
	RTFN                       = colour.ColorRed + "\n\tRead this fime manual\n" + colour.ColorReset
	ListOfPaths                = "Список доступных path для заметок:"
)
