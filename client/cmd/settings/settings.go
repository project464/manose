package settings

var (
	UserToken string
)

const (
	LocalSettingsPath = "~/.local/manose/manosecli.conf"
	ServerUrl         = "http://localhost:8000"
	NoteAPI           = ServerUrl + "/api/note"
	NotePathAPI       = ServerUrl + "/api/path"
)
