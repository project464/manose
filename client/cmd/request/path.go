package request

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"manosecli/cmd/check"
	"manosecli/cmd/model"
	"manosecli/cmd/settings"
	"net/http"
)

func RequestPathList() (mns []string) {
	log.Println("Request path list")
	resp, err := http.Get(settings.NotePathAPI)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	var content []byte
	if content, err = ioutil.ReadAll(resp.Body); err != nil {
		fmt.Println(err)
	}
	err = json.Unmarshal(content, &mns)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	return mns
}

func RequestPathShow(path string) (mn []model.Note) {
	requestUrl := settings.NotePathAPI + "/" + path
	resp, err := http.Get(requestUrl)

	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	var content []byte
	if content, err = ioutil.ReadAll(resp.Body); err != nil {
		fmt.Println(err)
	}
	err = json.Unmarshal(content, &mn)
	check.CheckError(err, check.ErrMarshaling)
	return mn
}

func RequestPathDelete(path string) string {
	client := &http.Client{}
	requestUrl := settings.NotePathAPI + "/" + path
	req, err := http.NewRequest(http.MethodDelete, requestUrl, nil)

	// Fetch Request
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return "'"
	}
	defer resp.Body.Close()

	var content []byte
	if content, err = ioutil.ReadAll(resp.Body); err != nil {
		fmt.Println(err)
	} else {
		return string(content)
	}
	return ""
}
