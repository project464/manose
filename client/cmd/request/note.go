package request

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"manosecli/cmd/check"
	"manosecli/cmd/model"
	"manosecli/cmd/settings"
	"net/http"
)

func RequestNotesList() (mns []model.Note) {
	resp, err := http.Get(settings.NoteAPI)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	var content []byte
	if content, err = ioutil.ReadAll(resp.Body); err != nil {
		fmt.Println(err)
	}

	err = json.Unmarshal(content, &mns)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	return mns
}

func RequestNoteAdd(mnin model.Note) (mnout model.Note) {
	fmt.Println(mnin)
	js, err := json.Marshal(mnin)
	fmt.Println(string(js))
	check.CheckError(err, check.ErrMarshaling)
	newNoteReader := bytes.NewReader(js)

	requestURl := settings.NoteAPI
	fmt.Println(requestURl)
	resp, err := http.Post(requestURl, "application/json", newNoteReader)

	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	fmt.Println(resp.Body)
	var content []byte
	if content, err = ioutil.ReadAll(resp.Body); err != nil {
		fmt.Println(err)
	}
	err = json.Unmarshal(content, &mnout)
	check.CheckError(err, check.ErrMarshaling)
	return mnout
}

func RequestNoteShow(id string) (mn model.Note) {
	requestUrl := settings.NoteAPI + "/" + id
	resp, err := http.Get(requestUrl)

	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	var content []byte
	if content, err = ioutil.ReadAll(resp.Body); err != nil {
		fmt.Println(err)
	}
	err = json.Unmarshal(content, &mn)
	check.CheckError(err, check.ErrMarshaling)
	return mn
}

func RequestNoteDelete(id string) string {
	client := &http.Client{}
	requestUrl := settings.NoteAPI + "/" + id
	log.Println(requestUrl)
	req, err := http.NewRequest(http.MethodDelete, requestUrl, nil)

	// Fetch Request
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return "'"
	}
	defer resp.Body.Close()

	var content []byte
	if content, err = ioutil.ReadAll(resp.Body); err != nil {
		fmt.Println(err)
	} else {
		return string(content)
	}
	return ""
}
