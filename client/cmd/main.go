/* ClientGet
 */
package main

import (
	"manosecli/cmd/command"
	"os"
)

func main() {
	command.CheckEmptyArgs(os.Args)
	command.Command(os.Args[1:])
}
