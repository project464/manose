package check

import (
	"log"
)

func CheckError(e error, s string) {
	if e != nil {
		log.Println(s, ":", e)
		panic(e)
	}
}
