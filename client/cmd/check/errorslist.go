package check

var ErrMarshaling = "Marshal error"
var ErrUnmarshaling = "Unmarshal error"
var ErrReadStringfromStdin = "Unnable to read from stdin"

//db
var ErrDbItemCreate = "Error creating item in db"

//filesystem
var ErrWorkDir = "Error retrieving current working directory"
var ErrCreateFile = "Error create file"
var ErrDeleteFile = "Error delete file"
var ErrReadFile = "Error delete file"

//cmd
var ErrRunCommand = "Error command run"

var ErrorRenderText = "Coudn't render markdwon as cli text"
