package model

import (
	"bufio"
	"fmt"
	"manosecli/cmd/check"
	"manosecli/cmd/printout/colour"
	"os"
	"os/exec"
	"runtime"
	"strings"

	"github.com/charmbracelet/glamour"
)

type Note struct {
	ID      uint   `json:"id,omitempty"`
	Time    string `json:"time,omitempty"`
	Path    string `json:"path"`
	Title   string `json:"title"`
	Content string `json:"content"`
}

func (n *Note) NewNote() {
	var err error
	filename := "content.txt"
	defer os.Remove(filename)

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter title: ")
	n.Title, err = reader.ReadString('\n')
	check.CheckError(err, check.ErrReadStringfromStdin)
	fmt.Print("Enter path: ")
	n.Path, err = reader.ReadString('\n')
	check.CheckError(err, check.ErrReadStringfromStdin)

	b, e := openFileForEdit(filename)
	if b {
		text, err := os.ReadFile(filename)
		check.CheckError(err, check.ErrReadFile)
		n.Content = string(text)
		check.CheckError(err, check.ErrDeleteFile)
	} else {
		check.CheckError(e, check.ErrDeleteFile)
	}

	n.Title = strings.TrimSuffix(strings.TrimSuffix(n.Title, "\n"), "\r")
	n.Path = strings.TrimSuffix(strings.TrimSuffix(n.Path, "\n"), "\r")

}

func (n *Note) UpdateNote() {
	var err error
	filename := "content.txt"
	defer os.Remove(filename)

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter title: ")
	n.Title, err = reader.ReadString('\n')
	check.CheckError(err, check.ErrReadStringfromStdin)
	fmt.Print("Enter path: ")
	n.Path, err = reader.ReadString('\n')
	check.CheckError(err, check.ErrReadStringfromStdin)

	b, e := openFileForEdit(filename)
	if b {
		text, err := os.ReadFile(filename)
		check.CheckError(err, check.ErrReadFile)
		n.Content = string(text)
		check.CheckError(err, check.ErrDeleteFile)
	} else {
		check.CheckError(e, check.ErrDeleteFile)
	}
}

func openFileForEdit(filename string) (bool, error) {

	f, err := os.Create(filename)
	f.Close()
	check.CheckError(err, check.ErrCreateFile)
	switch runtime.GOOS {
	case "windows":
		cmd := exec.Command("cmd", "/C "+filename)
		err = cmd.Run()
		check.CheckError(err, check.ErrRunCommand)
		return true, err
	case "linux":
		fmt.Println(runtime.GOOS)
		cmd := exec.Command("vim", filename)
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		err = cmd.Run()
		check.CheckError(err, check.ErrRunCommand)
		return true, err
	default:
		return false, nil
	}

}

func (n *Note) PrintForList() {
	fmt.Print(colour.ColorPurple, "-*-", colour.ColorReset)
	fmt.Print("Id: ", n.ID)
	fmt.Print("\tDate: ", n.Time)
	fmt.Print("\tTitle: ", n.Title, "")
	fmt.Println(colour.ColorRed, "***", colour.ColorReset)
}

func (n *Note) PrintoutNote() {
	fmt.Print(colour.ColorPurple, "-*-\n", colour.ColorReset)
	fmt.Print("Id: ", n.ID)
	fmt.Print("\tDate: ", n.Time, "\n")
	fmt.Print("Path: ", colour.ColorBlue, n.Path, colour.ColorReset, "\n")
	fmt.Print("Title: ", n.Title, "\n")
	fmt.Println("Content:")
	printoutContent(n.Content)
	fmt.Println(colour.ColorRed, "***\n\n", colour.ColorReset)
}

func printoutContent(str string) {
	text, err := glamour.Render(str, "dark")
	check.CheckError(err, check.ErrorRenderText)
	fmt.Println(text)

}
