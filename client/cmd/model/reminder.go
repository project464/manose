package model

import "time"

type Reminder struct {
	ID        uint      `json:"id" gorm:"primaryKey"`
	Starttime time.Time `json:"starttime" gorm:"starttime"`
	Endtime   time.Time `json:"endtime" gorm:"endtime"`
	Name      string    `json:"name" gorm:"name"`
	Note      string    `json:"note" gorm:"note"`
}
