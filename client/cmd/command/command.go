package command

import (
	"fmt"
	"log"
	"manosecli/cmd/command/handlers"
	"manosecli/cmd/message"
	"os"
)

func CheckEmptyArgs(s []string) {
	log.Println("Чекаем пустые аргументы")
	if len(s) == 0 {
		handlers.Help()
		os.Exit(1)
	}
}

func Command(args []string) {
	CheckEmptyArgs(args)
	switch args[0] {
	case "note":
		notes(args[1:])
	case "reminder":
		notes(args[1:])
	default:
		handlers.NeedToChooseNoteOrReminder()

	}
}

func notes(args []string) {
	if len(args) == 0 {
		handlers.ListNotes()
	} else {
		CheckEmptyArgs(args)
		switch args[0] {
		case "add":
			handlers.AddNote()
		case "delete":
			log.Println(args[0])
			if len(args) == 2 {
				handlers.DeleteNote(args[1])
			} else {
				fmt.Println(message.NeedPathID)
				handlers.Help()
			}
		case "update":
			handlers.UpdateNote()
		case "path":
			paths(args[1:])
		default:
			if len(args) > 1 {
				fmt.Println(message.NeedNoteID)
				handlers.Help()
			} else {
				handlers.ShowNote(args[0])
			}
		}
	}
}

func paths(args []string) {
	if len(args) == 0 {
		handlers.ListPaths()
	} else {
		CheckEmptyArgs(args)
		switch args[0] {
		case "show":
		case "delete":
			log.Println(args[0])
			if len(args) == 2 {
				handlers.DeletePath(args[1])
			} else {
				fmt.Println(message.NeedPathID)
				handlers.Help()
			}
		default:
			log.Println(args[0])
			if len(args) > 1 {
				fmt.Println(message.NeedPathID)
				handlers.Help()
			} else {
				handlers.ShowPathNotes(args[0])
			}
		}
	}
}

func reminders(args []string) {
	CheckEmptyArgs(args)
	switch args[0] {
	case "add":
		handlers.AddReminder()
	case "delete":
		handlers.DeleteReminder()
	case "update":
		handlers.UpdateReminder()
	case "day":
		handlers.DayReminders()
	case "week":
		handlers.WeekReminders()
	case "month":
		handlers.MonthReminders()
	default:
		handlers.Help()
	}

}
