package handlers

import (
	"fmt"
	"manosecli/cmd/message"
)

func Help() {
	fmt.Println(message.RTFN)
}

func NeedToChooseNoteOrReminder() {
	fmt.Println(message.NeedToChooseNoteOrReminder)
}
