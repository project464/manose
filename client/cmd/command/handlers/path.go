package handlers

import (
	"fmt"
	"log"
	"manosecli/cmd/message"
	"manosecli/cmd/request"
)

func ShowPathNotes(path string) {
	log.Println("ShowPaths")
	mn := request.RequestPathShow(path)
	for _, note := range mn {
		note.PrintForList()
	}
}
func DeletePath(path string) {
	amountOfDeletedNotes := request.RequestPathDelete(path)
	fmt.Println(amountOfDeletedNotes, " notes has been deleted")
}
func ListPaths() {
	mns := request.RequestPathList()
	fmt.Println(message.ListOfPaths)
	for i, path := range mns {
		fmt.Print("\t", i, ") ")
		fmt.Println(path)
	}

}
