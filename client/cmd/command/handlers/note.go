package handlers

import (
	"log"
	"manosecli/cmd/message"
	"manosecli/cmd/model"
	"manosecli/cmd/request"

	"fmt"
)

func AddNote() {
	var mn model.Note
	mn.NewNote()

	note := request.RequestNoteAdd(mn)
	note.PrintForList()

}
func ShowNote(id string) {
	log.Println("ShowNote")
	mn := request.RequestNoteShow(id)
	if mn.ID == 0 {
		fmt.Println(message.WrongNoteID)
	} else {
		mn.PrintoutNote()
	}
}
func DeleteNote(id string) {
	amountOfDeletedNotes := request.RequestNoteDelete(id)
	fmt.Println(amountOfDeletedNotes, " notes has been deleted")
}
func UpdateNote() {}
func ListNotes() {
	mns := request.RequestNotesList()
	for _, note := range mns {
		note.PrintForList()
	}

}
