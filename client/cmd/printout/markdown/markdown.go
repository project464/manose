package markdown

import (
	"fmt"
	"manosecli/cmd/printout/colour"
	"strings"
)

func PrintLine(line string) {
	if strings.Contains(line, "# ") {
		fmt.Println("\n", colour.ColorRed, strings.Trim(line, "# "), colour.ColorReset, "\n")
		return
	}
	fmt.Println(line)
}
