package check

var ErrMarshaling = "Marshal error"
var ErrUnmarshaling = "Unmarshal error"

//db
var ErrDbItemCreate = "Error creating item in db"

//filesyste
var ErrWorkDir = "Error retrieving current working directory"
