package route

import (
	"manose/cmd/webserver/handlers"

	"github.com/gofiber/fiber/v2"
)

func Api(app *fiber.App) {
	api := app.Group("/api")
	//***
	//notes
	//***
	note := api.Group("/note")
	note.Get("/:id?", handlers.GetNote)
	note.Post("/", handlers.CreateNote)
	note.Put("/:id", handlers.UpdateNote)
	note.Delete("/:id", handlers.DeleteNote)

	path := api.Group("/path")
	path.Get("/:path?", handlers.GetPaths)
	path.Delete("/:path", handlers.DeletePath)

	//***
	//reminders
	//***
	reminder := api.Group("/reminder")
	reminder.Get("/reminder/:id?", handlers.GetReminder)
	reminder.Post("/reminder", handlers.CreateReminder)
	reminder.Put("/reminder/:id", handlers.UpdateReminder)
	reminder.Delete("/reminder/:id", handlers.DeleteReminder)

}
