package webserver

import (
	"manose/cmd/webserver/route"

	"github.com/gofiber/fiber/v2"
)

func StartWebServer() *fiber.App {

	//Создаем и задаем машруты для веб сервиса
	app := fiber.New()

	route.Api(app)

	app.Static("/", "./www")

	return app
}
