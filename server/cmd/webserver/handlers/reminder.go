package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"manose/cmd/check"
	"manose/cmd/model"
	"manose/cmd/settings"

	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

//функционал для webserver
func CreateReminder(c *fiber.Ctx) error {
	var mr model.Reminder

	err := json.Unmarshal([]byte(c.Body()), &mr)
	check.CheckError(err, "Не распарсить")
	if err != nil {
		c.Status(500).SendString(fmt.Sprint(err))
		return err
	}

	log.Println("saving")
	reminderCreate := settings.DBconn.Create(&mr)
	check.CheckError(reminderCreate.Error, check.ErrDbItemCreate)
	if err != nil {
		c.Status(500).SendString(fmt.Sprint(err))
		return err
	}

	js, err := json.Marshal(reminderCreate)
	check.CheckError(err, check.ErrMarshaling)

	c.Send(js)
	return nil

}

func GetReminder(c *fiber.Ctx) error {
	var mrs []model.Reminder
	var id string
	var res *gorm.DB
	if c.Params("id") != "" {
		id = c.Params("id")
		res = settings.DBconn.First(&mrs, id)
	} else {
		res = settings.DBconn.Find(&mrs)
	}
	if mrs == nil && res.Error == nil {
		c.SendStatus(404)
		return errors.New("Too much nils")
	}
	if res.Error != nil {
		c.Status(500).SendString(fmt.Sprint(res.Error))
		return res.Error
	}

	json, _ := json.Marshal(mrs)
	c.Send(json)
	return nil
}

func UpdateReminder(c *fiber.Ctx) error {
	var mr model.Reminder
	err := json.Unmarshal([]byte(c.Body()), &mr)
	if err != nil {
		c.Status(500).SendString(fmt.Sprint(err))
		return err
	}

	res := settings.DBconn.Model(&mr).Updates(mr)
	if res.Error != nil {
		c.Status(500).SendString(fmt.Sprint(res.Error))
		return res.Error
	}

	c.SendString(fmt.Sprint(mr))
	return nil
}

func DeleteReminder(c *fiber.Ctx) error {
	var mr model.Reminder
	id := c.Params("id")
	if id != "" {
		c.Status(500)
		return errors.New("Id is emtpty")
	}

	res := settings.DBconn.Delete(&mr, id)
	if res.Error != nil {
		c.Status(500).SendString(fmt.Sprint(res.Error))
		return res.Error
	}
	c.SendString(fmt.Sprint(mr))
	return nil

}
