package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"manose/cmd/check"
	"manose/cmd/model"
	"manose/cmd/settings"
	"time"

	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

//функционал для webserver
func CreateNote(c *fiber.Ctx) error {
	var mn model.Note
	mn.Time = time.Now().Format(settings.AppConfig.Webserver.TimeFormat)

	err := json.Unmarshal([]byte(c.Body()), &mn)
	check.CheckError(err, check.ErrUnmarshaling)
	if err != nil {
		c.Status(500).SendString(fmt.Sprint(err))
		return err
	}

	log.Println("saving")
	noteCreationResponse := settings.DBconn.Create(&mn)
	check.CheckError(noteCreationResponse.Error, check.ErrDbItemCreate)
	if err != nil {
		log.Println("saving fail")
		c.Status(500).SendString(fmt.Sprint(err))
		return err
	}

	js, err := json.Marshal(&mn)
	check.CheckError(err, check.ErrMarshaling)

	c.Send(js)
	return nil

}

func GetNote(c *fiber.Ctx) error {
	var mns []model.Note
	var id string
	var res *gorm.DB
	if c.Params("id") != "" {
		id = c.Params("id")
		res = settings.DBconn.First(&mns, id)
	} else {
		res = settings.DBconn.Find(&mns)
	}
	if mns == nil && res.Error == nil {
		c.SendStatus(404)
	}
	if res.Error != nil {
		c.Status(500).SendString(fmt.Sprint(res.Error))
		return res.Error
	}

	if len(mns) == 1 {
		json, err := json.Marshal(mns[0])
		if res.Error != nil {
			c.Status(500).SendString(fmt.Sprint(err))
			return err
		}
		c.Send(json)
		return nil
	}

	json, err := json.Marshal(mns)
	if res.Error != nil {
		c.Status(500).SendString(fmt.Sprint(err))
		return err
	}
	c.Send(json)
	return nil
}

func UpdateNote(c *fiber.Ctx) error {
	var mn model.Note
	err := json.Unmarshal([]byte(c.Body()), &mn)
	if err != nil {
		c.Status(500).SendString(fmt.Sprint(err))
		return err
	}

	log.Println("update")
	res := settings.DBconn.Model(&mn).Updates(mn)
	if res.Error != nil {
		log.Println("update fail")
		c.Status(500).SendString(fmt.Sprint(res.Error))
		return res.Error
	}

	c.SendString(fmt.Sprint(mn))
	return nil
}

func DeleteNote(c *fiber.Ctx) error {
	var mn model.Note
	id := c.Params("id")
	if id == "" {
		c.Status(500)
		return errors.New("Id is empty")
	}

	log.Println("delete")
	res := settings.DBconn.Delete(&mn, id)
	if res.Error != nil {
		log.Println("delete fail")
		c.Status(500).SendString(fmt.Sprint(res.Error))
		return res.Error
	}
	c.SendString(fmt.Sprint(res.RowsAffected))
	return nil

}
