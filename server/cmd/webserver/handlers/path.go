package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"manose/cmd/model"
	"manose/cmd/settings"
	"strings"

	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

//функционал для webserver

func GetPaths(c *fiber.Ctx) error {
	log.Println("GetPaths")
	var paths []string
	var mns []model.Note
	var id string
	var res *gorm.DB

	if c.Params("path") != "" {
		id = c.Params("path")
		search := strings.Replace(id, "%20", " ", 3)
		log.Println("path = ", search)
		res = settings.DBconn.Find(&mns, "path = ?", search)
	} else {
		res = settings.DBconn.Model(&mns).Distinct().Pluck("path", &paths)
	}

	if mns == nil && &paths == nil && res.Error == nil {
		c.SendStatus(404)
		return errors.New("Empty response from db")
	}
	if res.Error != nil {
		c.Status(500).SendString(fmt.Sprint(res.Error))
		return res.Error
	}

	if mns != nil {
		json, err := json.Marshal(mns)
		if err != nil {
			c.Status(500).SendString(fmt.Sprint(err))
			return err
		}
		c.Send(json)
		return nil
	}
	if paths != nil {
		json, err := json.Marshal(paths)
		if err != nil {
			c.Status(500).SendString(fmt.Sprint(err))
			return err
		}
		c.Send(json)
		return nil
	}
	c.Status(500)
	return nil
}

func GetPathNotes(c *fiber.Ctx) error {
	var mns []model.Note
	var path string
	var res *gorm.DB
	if c.Params("path") != "" {
		path = c.Params("path")
		res = settings.DBconn.First(&mns, path)
	} else {
		res = settings.DBconn.Find(&mns)
	}
	if mns == nil && res.Error == nil {
		c.SendStatus(404)
	}
	if res.Error != nil {
		c.Status(500).SendString(fmt.Sprint(res.Error))
		return res.Error
	}

	if len(mns) == 1 {
		json, err := json.Marshal(mns[0])
		if res.Error != nil {
			c.Status(500).SendString(fmt.Sprint(err))
			return err
		}
		c.Send(json)
		return nil
	}

	json, err := json.Marshal(mns)
	if res.Error != nil {
		c.Status(500).SendString(fmt.Sprint(err))
		return err
	}
	c.Send(json)
	return nil
}

func GetPathNotes_remove(c *fiber.Ctx) error {
	var mn model.Note
	err := json.Unmarshal([]byte(c.Body()), &mn)
	if err != nil {
		c.Status(500).SendString(fmt.Sprint(err))
		return err
	}

	log.Println("update")
	res := settings.DBconn.Model(&mn).Updates(mn)
	if res.Error != nil {
		log.Println("update fail")
		c.Status(500).SendString(fmt.Sprint(res.Error))
		return res.Error
	}

	c.SendString(fmt.Sprint(mn))
	return nil
}

func DeletePath(c *fiber.Ctx) error {
	var mns []model.Note
	var path string
	var res *gorm.DB
	if c.Params("path") != "" {
		path = c.Params("path")
		search := strings.Replace(path, "%20", " ", 3)
		log.Println("path = ", search)
		res = settings.DBconn.Find(&mns, "path = ?", search)
	} else {
		c.Status(500)
		return errors.New("Path is empty")
	}
	if mns == nil && res.Error == nil {
		c.SendStatus(404)
	}
	if res.Error != nil {
		c.Status(500).SendString(fmt.Sprint(res.Error))
		return res.Error
	}

	log.Println("Deleting")

	fmt.Println(len(mns))

	if len(mns) == 0 {
		return errors.New("0")
	} else {
		res = settings.DBconn.Delete(&mns)
		if res.Error != nil {
			log.Println("delete fail")
			c.Status(500).SendString(fmt.Sprint(res.Error))
			return res.Error
		}
	}
	c.SendString(fmt.Sprint(res.RowsAffected))
	return nil

}
