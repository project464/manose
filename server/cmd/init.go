package main

import (
	"manose/cmd/check"
	"manose/cmd/db"
	"manose/cmd/settings"
	"manose/cmd/webserver"
	"os"
	"path/filepath"
)

func init() {

	p, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		check.CheckError(err, check.ErrWorkDir)
	}
	settings.WorkDir = p

	settings.DBconn = db.InitDB()
	//Конфигурирование веб сервера
	settings.WebApp = webserver.StartWebServer()
	//Применение настроек к состоянию системы.
	//networksettings := models.GetNetworkConfiguration()
	//configurator.ApplyNewNetworkSettingsAfterUpdate(&networksettings)

}
