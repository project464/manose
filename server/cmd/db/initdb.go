package db

import (
	"log"
	"manose/cmd/check"
	"manose/cmd/settings"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func initDBConn() *gorm.DB {
	if settings.DBconn != nil {
		return settings.DBconn
	} else {
		return initDB(settings.AppConfig.Database.DbFile)
	}
}

func initDB(filepath string) *gorm.DB {
	DBconn, err := gorm.Open(sqlite.Open(filepath), &gorm.Config{})
	if err != nil || DBconn == nil {
		log.Println(err)
		log.Println(DBconn)
		check.CheckError(err, "Unable to connect to db")
	}
	log.Println("Database Connected")
	return DBconn
}
