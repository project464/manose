package db

import (
	"gorm.io/gorm"
)

func InitDB() *gorm.DB {
	conn := initDBConn()
	migrate(conn)
	return conn
}
