package db

import (
	"manose/cmd/model"

	"gorm.io/gorm"
)

func migrate(dbc *gorm.DB) {
	dbc.AutoMigrate(&model.Note{})
	dbc.AutoMigrate(&model.Reminder{})
}
