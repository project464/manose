package model

import "time"

type Note struct {
	ID      uint   `json:"id" gorm:"primaryKey"`
	Time    string `json:"time" gorm:"time"`
	Path    string `json:"path" gorm:"path"`
	Title   string `json:"title" gorm:"path"`
	Content string `json:"content" gorm:"content"`
}

type Reminder struct {
	ID        uint      `json:"id" gorm:"primaryKey"`
	Starttime time.Time `json:"starttime" gorm:"starttime"`
	Endtime   time.Time `json:"endtime" gorm:"endtime"`
	Title     string    `json:"title" gorm:"name"`
	Note      string    `json:"note" gorm:"note"`
}
