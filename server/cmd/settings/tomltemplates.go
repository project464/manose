package settings

import (
	"manose/cmd/check"

	"github.com/BurntSushi/toml"
)

type TomlAppConfig struct {
	Database struct {
		DbFile string `toml:"DbFile"`
	} `toml:"database"`

	Webserver struct {
		AdressAndPort string `toml:"AdressAndPort"`
		TimeFormat    string `toml:"TimeFormat"`
	} `toml:"webserver"`
}

func (t *TomlAppConfig) ReadTomlAppConfig(path string) TomlAppConfig {

	_, err := toml.DecodeFile(path, &t)
	check.CheckError(err, "Ошибка чтения файла")
	return *t

}
