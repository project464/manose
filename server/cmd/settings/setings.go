package settings

import (
	"path"

	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

const (
	TomltemplatesPath = "configs"
	AppConfigName     = "appconfig.toml"
)

var AppConfig = (&TomlAppConfig{}).ReadTomlAppConfig(path.Join(TomltemplatesPath, AppConfigName))

var DBconn *gorm.DB
var WorkDir string
var WebApp *fiber.App
