package main

import (
	"manose/cmd/check"
	"manose/cmd/settings"
)

func main() {
	listenerChan := make(chan bool)
	existChan := make(chan bool)

	//Стартуем веб сервер
	go func(exitChan chan bool) {
		defer func() {
			existChan <- true
		}()
		go runWeb()
	}(listenerChan)

	<-listenerChan

}

func runWeb() {
	err := settings.WebApp.Listen(settings.AppConfig.Webserver.AdressAndPort)
	check.CheckError(err, "error run web server")
}
