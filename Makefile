
.ONESHELL:

#server
serverrun: serverbuild
	cd build/server
	./manose

serverbuild:
	cd ./server
	mkdir -p ../build/server/ || true
	go build -o manose ./cmd/
	cp -r configs ../build/server/configs
	mv manose ../build/server/

#client
clientrun: clientbuild
	cd build/client
	./manosecli http://localhost:8000/api/note/13

clientbuild:
	cd ./client
	mkdir -p ../build/client/ || true
	go build -o manosecli ./cmd/
	mv manosecli ../build/client

#docker-compose
up: build
	touch manose.db || true
	docker-compose up
upd: build
	touch manose.db || true
	docker-compose up -d


build:
	docker-compose build --no-cache
	
down:
	docker-compose down

noteadd:
	cd ./server
	curl -X POST -d "@data/note.json" localhost:8000/api/note
